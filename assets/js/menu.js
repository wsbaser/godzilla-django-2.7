/**
 * Created by User on 24.07.14.
 */
function Menu(rootEl,imgDir) {
    this.rootEl = rootEl;
    this.imgDir = imgDir || 'static/img/menu/';
    this.items = [];
    this.mouseOverTime=null;
}

Menu.prototype = {
    isTransparentUnderMouse:function(item, evnt) {
        var target = item.itemEl[0];
        var l = 0, t = 0;
        if (target.offsetParent) {
            var ele = target;
            do {
                l += ele.offsetLeft;
                t += ele.offsetTop;
            } while (ele = ele.offsetParent);
        }
        var x = evnt.pageX - l;
        var y = evnt.pageY - t;
        var imgData = target.getContext('2d').getImageData(x, y, 1, 1);
        var data = imgData.data;
        if (
            data[0] == 0 &&
            data[1] == 0 &&
            data[2] == 0 &&
            data[3] == 0
            ) {
            return true;
        }
        return false;
    },
    //=========================================================================
    //  click
    //=========================================================================
    onItemClick:function(e, item, prevItem) {
        var isOnItem = !this.isTransparentUnderMouse(item, e);
        var isOnPrevItem = !isOnItem && prevItem && !this.isTransparentUnderMouse(prevItem, e);
        var activeItem = isOnItem ?
            item :
            (isOnPrevItem ? prevItem : null);
        if (!activeItem)
            return;

        if (activeItem.isDropDown) {
            this.hideAllDropMenu();
            this.showMenuItems(activeItem);
            if(e.stopPropagation)
                e.stopPropagation();
        }
        else
            window.location = activeItem.url;
    },
    //=========================================================================
    //  init
    //=========================================================================
    init: function() {
        var prevItem = null;
        $.each(this.items,function(i, item){
            this.initItem(item, prevItem);
            prevItem = item;
        }.bind(this));
    },
    addItem:function(id,data) {
        var item = {
            id: id,
            isRoot: true,
            isDropDown: typeof(data) == 'object',
            itemEl: $('#' + id),
            parent: this
        };
        this.items.push(item);
        if (item.isDropDown) {
            item.items = data;  // элемент меню с выпадающим списком
            $.each(item.items, function (i, dropItem) {
                dropItem.parent = item;
            });
        }
        else
            item.url = data;
    },
    initDropItems:function(item) {
        $.each(item.items, function (i, dropItem) {
            var dropItemEl = $('<canvas id="' + dropItem.id + '" ' +
                'width="260" height="84"></canvas>');
            dropItemEl.css('width', '260');
            dropItemEl.css('height', '84');
            dropItemEl.css('display', 'none');
            dropItemEl.css('z-index',1000);
            this.setItemPosition(item.itemEl, dropItemEl, i);
            this.rootEl.append(dropItemEl);
            dropItem.itemEl = dropItemEl;
            this.initItem(dropItem);
        }.bind(this));
    },
    setItemPosition:function(itemEl,dropItemEl,i) {
        var xShift = 40;
        var yShift = 84;
        var x =itemEl[0].offsetLeft -               // точка отсчета - элемент корневого меню
            72+                                     // константа смещения
            (i<2?(-i * xShift):(i - 3) * xShift);   // ступеньки
        var y = itemEl[0].offsetTop + itemEl[0].height + i * yShift;
        dropItemEl.css('position', 'absolute');
        dropItemEl.css('left', x);
        dropItemEl.css('top', y);
    },
    getItemSrc:function(item,active) {
        return this.imgDir + item.itemEl[0].id + (active ? '_active' : '') + '.gif';
    },
    initItem: function (item, prevItem) {
        this.drawItem(item, false);
        var itemEl = item.itemEl;
        itemEl.on('click', function (e) {
            this.onItemClick(e, item, prevItem);
        }.bind(this));
        itemEl.on('mousemove', function (e) {
            this.onItemMouseMove(e, item, prevItem)
        }.bind(this));
        itemEl.on('mouseout', function (e) {
            this.onItemMouseOut(item);
        }.bind(this));
        if (item.items)
            this.initDropItems(item);
    },
    //=========================================================================
    //  draw
    //=========================================================================
    drawItem: function(item, makeActive) {
        var itemEl = item.itemEl;
        var isActiveNow = itemEl.data('active');
        if ((makeActive && isActiveNow === true) ||
            (!makeActive && isActiveNow === false))
            return;
        var ctx = itemEl[0].getContext('2d');
        var img = new Image();
        img.onload = function () {
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.drawImage(img, 0, 0);
        };
        img.src = this.getItemSrc(item, makeActive);
        itemEl.data('active', makeActive);
    },
    //=========================================================================
    //  activate
    //=========================================================================
    activateItem:function(item, prevItem, activate) {
        if (activate) {
            // деактивируем все остальные элементы меню
            $.each(item.parent.items, function (i, sibling) {
                this.drawItem(sibling, sibling.id == item.id);
            }.bind(this));
            if (item.isDropDown) {
                window.setTimeout(function () {
                    this.onItemClick({pageX: document.Show.MouseX.value, pageY: document.Show.MouseY.value}, item, null);
                }.bind(this), 300);
            }
        }
        else {
            this.drawItem(item, false);
            if (item.isDropDown || !item.isRoot) {
                // если деактивируется элемент выпадающего меню
                // или элемент корневого меню с выпадающим меню(а не с прямой ссылкой)
                this.hideDropMenuAfterTimeout(item.isDropDown ? item : item.parent);
            }
        }
    },
    hideDropMenuAfterTimeout:function(item) {
        window.setTimeout(function () {
            if (!item.itemsVisible)
                return;
            // если курсор находится не на элементе корневого меню и не на
            // элементе выпадающего списка - скрыть все элементы выпадающего списка
            var e = this.getCursorPosition();
            var isOnItem = !this.isTransparentUnderMouse(item, e);
            for (var i = 0; i < item.items.length && !isOnItem; i++) {
                if(!this.isTransparentUnderMouse(item.items[i], e))
                    isOnItem=true;
            }
            if (!isOnItem)
                this.hideDropMenu(item);
        }.bind(this), 700);
    },
    getCursorPosition:function() {
        return {
            pageX: document.Show.MouseX.value,
            pageY: document.Show.MouseY.value
        };
    },
    //=========================================================================
    //  mouse out
    //=========================================================================
    onItemMouseOut:function(item) {
        this.activateItem(item, null, false);
    },
    //=========================================================================
    //  mouse move
    //=========================================================================
    onItemMouseMove:function(e,item,prevItem) {
        var isOnItem = !this.isTransparentUnderMouse(item, e);
        var isOnPrevItem = !isOnItem && prevItem && !this.isTransparentUnderMouse(prevItem, e);

        item.itemEl.css('cursor', isOnItem || isOnPrevItem ? 'pointer' : 'default');

        this.activateItem(item, prevItem, isOnItem);
        if (prevItem)
            this.activateItem(prevItem, null, isOnPrevItem);
    },
    //=========================================================================
    // show/hide drop menu
    //=========================================================================
    showMenuItems:function(parent) {
        $.each(parent.items, function (i, item) {
            this.drawItem(item, false);
            item.itemEl.show();
        }.bind(this));
        parent.itemsVisible=true;
    },
    hideAllDropMenu:function() {
        $.each(this.items, function (i, item) {
            this.hideDropMenu(item);
        }.bind(this));
    },
    hideDropMenu:function(item) {
        if (item.itemsVisible) {
            $.each(item.items, function (i, dropItem) {
                dropItem.itemEl.hide();
            });
            item.itemsVisible = false;
        }
    },
    firstItem:function() {
        return this.items[0];
    },
    getItems:function() {
        return this.items;
    }
};
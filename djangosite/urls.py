from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangosite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'main.views.home', name='godzilla_home'),
    url(r'^photos$', 'main.views.photos', name='godzilla_photos'),
    url(r'^videos$', 'main.views.videos', name='godzilla_videos'),
    url(r'^contacts$', 'main.views.contacts', name='godzilla_contacts'),
    url(r'^actions$', 'main.views.actions', name='godzilla_actions'),
    url(r'^contract$', 'main.views.contract', name='godzilla_contract'),
    url(r'^corporative$', 'main.views.corporative', name='godzilla_corporative'),
    url(r'^equipment$', 'main.views.equipment', name='godzilla_equipment'),
    url(r'^family$', 'main.views.family', name='godzilla_family'),
    url(r'^gifts$', 'main.views.gifts', name='godzilla_gifts'),
    url(r'^motorcycles$', 'main.views.motorcycles', name='godzilla_motorcycles'),
    url(r'^photoescort$', 'main.views.photoescort', name='godzilla_photoescort'),
    url(r'^prices$', 'main.views.prices', name='godzilla_prices'),
    url(r'^quadbikes$', 'main.views.quadbikes', name='godzilla_quadbikes'),
    url(r'^routes$', 'main.views.routes', name='godzilla_routes'),
    url(r'^rules$', 'main.views.rules', name='godzilla_rules'),
    url(r'^safety$', 'main.views.safety', name='godzilla_safety'),
    url(r'^school$', 'main.views.school', name='godzilla_school'),
    url(r'^service$', 'main.views.service', name='godzilla_service'),
    url(r'^shipping$', 'main.views.shipping', name='godzilla_shipping'),
)

from django.shortcuts import render, redirect

# Create your views here.
def home(request):
    return render(request, "main/home.html")


def photos(request):
    return render(request, "main/photos.html")


def videos(request):
    return render(request, "main/videos.html")


def contacts(request):
    return render(request, "main/contacts.html")


def actions(request):
    return render(request, "main/actions.html")


def contract(request):
    return render(request, "main/contract.html")


def corporative(request):
    return render(request, "main/corporative.html")


def equipment(request):
    return render(request, "main/equipment.html")


def family(request):
    return render(request, "main/family.html")


def gifts(request):
    return render(request, "main/gifts.html")


def motorcycles(request):
    return render(request, "main/motorcycles.html")


def photoescort(request):
    return render(request, "main/photoescort.html")


def prices(request):
    return render(request, "main/prices.html")


def quadbikes(request):
    return render(request, "main/quadbikes.html")


def routes(request):
    return render(request, "main/routes.html")


def rules(request):
    return render(request, "main/rules.html")


def safety(request):
    return render(request, "main/safety.html")


def school(request):
    return render(request, "main/school.html")


def service(request):
    return render(request, "main/service.html")


def shipping(request):
    return render(request, "main/shipping.html")